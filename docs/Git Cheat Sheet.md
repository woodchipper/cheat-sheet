# Git Cheat Sheet

## Configuration
```
// Store credentials
git config credential.helper store
```

## Branches

### Create
```
// Local
git checkout -b <branch_name>

// Enable Remote Tracking
git push -u origin <branch_name>
```

### Delete
```
// Local
git branch -d <branch_name>

// Remote
git push origin --delete <branch_name>
git push origin :<branch_name>
``` 

### Update List
```
git fetch -p origin
```
